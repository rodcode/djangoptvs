from rest_framework.decorators import api_view
from rest_framework.response import Response
from app.models import Poll
from app.api.serializers import PollSerializer


@api_view(['GET'])
def poll_list(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        polls = Poll.objects.all()
        serializer = PollSerializer(polls, many=True)
        return Response(serializer.data)

from rest_framework import serializers
from app.models import Poll

class PollSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(PollSerializer, self).__init__(*args, **kwargs)

    class Meta(object):
        model = Poll
        fields = ['text']
import json
import django
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from app.base_tests import BaseTestCase

class PoolsApiTest(BaseTestCase):
    
    def setUp(self):
        self.client.get('/seed')

    def test_create_account(self):
        """
        Ensure we can get polls
        """
        client = APIClient()
        response = client.get('/api/polls/')
        json_response = json.loads(response.content)
        self.assertEqual(len(json_response), 3)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

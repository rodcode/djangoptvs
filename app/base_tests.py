from django.test import TestCase

import django

class BaseTestCase(TestCase):

    def __init__(self, *args, **kwargs):
        super(BaseTestCase, self).__init__(*args, **kwargs)

    @classmethod
    def setUpClass(cls):
        super(BaseTestCase, cls).setUpClass()
        django.setup()